#!/usr/bin/env bash

CFG=/config/seadrive.conf
TPL=/home/seadrive/seadrive.tpl.conf

function ask_for() {
  echo -n "$1" 1>&2
  if [[ "" == "$2" ]]; then
    read v
  else
    read -s v
    echo -n "" 1>&2
  fi
  echo "$v"
}

token=
client_name=calfater-seafile-seadrive-$(hostname)
while [[ "" = "$token" ]]; do
  server=$(ask_for "Server url : ")
  username=$(ask_for "Username (email) : ")
  password=$(ask_for "Password : " 1)
  token=$(curl -d "username=$username" -d "password=$password" "$server/api2/auth-token/" | sed -e 's/.*:"//' -e 's/"}//' | grep -e [a-f0-9])
done

sed \
  -e 's,the-server,'"$server"',g' \
  -e 's,the-username,'"$username"',g' \
  -e 's,the-password,'"$password"',g' \
  -e 's,the-token,'"$token"',g' \
  -e 's,the-client_name,'"$client_name"',g' \
  "$TPL" > "$CFG"

chown seadrive\: "$CFG"
