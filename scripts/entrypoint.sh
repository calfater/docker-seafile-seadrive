#!/usr/bin/env bash
if [[ "noop" = "$1" ]]; then
  echo "noop"
  exit
fi
echo "--------------------------------------------------------------------------------"
echo "User  : $UNAME ($UID)"
echo "Group : $GNAME ($GID)"

echo -e "$UNAME:x:${UID}:${GID}:$UNAME:/home/$UNAME:/bin/bash\n" >> /etc/passwd
echo -e "$GNAME:x:${GID}:$GNAME\n" >> /etc/group

chown $UNAME\: /drive
chown -R $UNAME\: /config /home/$UNAME

#bash -c "/home/$UNAME/script.sh $@"
sudo -i -u $UNAME bash -c "/home/$UNAME/script.sh $@"
