#!/usr/bin/env bash

CFG=/config/seadrive.conf

if [[ ! -f "$CFG" ]]; then
  echo "Waiting for config file"
  echo "You can : "
  echo " 1)"
  echo "   - run : docker exec --interactive --tty seafile-seadrive bash /configure"
  echo "or : "
  echo " 2)"
  echo "   - edit '/config/seadrive.conf'"
  echo "   - get a token : curl -d \"username=username@example.com\" -d \"password=123456\" https://seafile.example.com/api2/auth-token/"
fi

while [[ ! -f "$CFG" ]]; do
  echo "Waiting for config file"
  sleep 5
done

echo "Starting Seadrive"
seadrive -f -c /config/seadrive.conf -d /config/data /drive
echo "End of the script (and probably Docker)"
